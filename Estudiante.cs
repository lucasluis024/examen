using System;
namespace EXAMEN;
class Estudiante
{
    public string nombre {get; set;}
    public string apellido {get; set;}
    public string nombre_ins{get; set;}
    public int cedula_identidad{get; set;}
    public void Estudiante(string nombre, string apellido, string nombre_ins, int cedula){
        this.nombre=nombre;
        this.apellido=apellido;
        this.nombre_ins=nombre_ins;
        this.cedula_identidad=cedula;
    }
    public virtual void mostrar_datos(){}
    public virtual void calcular_precio(){}
}